/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <brett@librecast.net> */
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <libgen.h>
#include <librecast.h>
#include <librecast/if.h>
#include <limits.h>
#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/ipv6.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netpacket/packet.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define RELAY_PORT_UDP 2268
#define IPV6_BYTES 16

struct udp6hdr {
	struct in6_addr src;
	struct in6_addr dst;
	uint32_t udplen;
	uint8_t zero[3];
	uint8_t nxt;
	uint16_t sport;
	uint16_t dport;
	uint16_t len;
	uint16_t csum;
	char data[];
};

typedef struct lc_peer_s lc_peer_t;
struct lc_peer_s {
	lc_peer_t              *next;
	struct timespec         seen; /* last seen */
	struct sockaddr_storage addr;
	char                    type; /* 0 = static peer (configured relay), 1 = dynamic */
};

typedef struct lc_interface_s lc_interface_t;
struct lc_interface_s {
	char ifname[IFNAMSIZ];
	struct sockaddr hwaddr;
	pthread_t thread;
	unsigned int ifx;
	int mtu;
	int sock;
};

typedef struct lc_relay_s lc_relay_t;
struct lc_relay_s {
	lc_ctx_t *ctx;
	struct lc_interface_s if_wan; /* WAN interface */
	struct lc_interface_s if_tun; /* tunnel interface */
	lc_peer_t              *peer;
};

enum {
	TUN,
	WAN
};
static char *wan_iface;
static char *progname;
static char *bridge;
static volatile int running = 1;

static void usage(int rc)
{
	printf("usage:\n\t%s [-b bridge] wan_iface [relay_ip] ...\n", progname);
	_exit(rc);
}

static void handle_sigint(int signo)
{
	(void) signo;
	running = 0;
}

static void dumpmac(FILE *f, char *pre, unsigned char mac[ETH_ALEN])
{
	fprintf(f, "%smac: %02hhX-%02hhX-%02hhX-%02hhX-%02hhX-%02hhX\n", pre,
		mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

static void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/* initialize struct - set ifname from ifx or vice-versa */
static inline int interface_init(struct lc_interface_s *lci)
{
	struct ifreq ifr = {0};

	if (!lci->ifx)
		lci->ifx = if_nametoindex(lci->ifname);
	else if (!lci->ifname[0] && lci->ifx)
		if_indextoname(lci->ifx, lci->ifname);
	else {
		errno = EINVAL;
		return -1;
	}
	strncpy(ifr.ifr_name, lci->ifname, sizeof ifr.ifr_name);
	fprintf(stderr, "getting hwaddr for '%s'\n", lci->ifname);
	if (ioctl(lci->sock, SIOCGIFHWADDR, &ifr) == -1) {
		perror("ioctl(SIOCGIFHWADDR)");
		return -1;
	}
	memcpy(&lci->hwaddr, &ifr.ifr_hwaddr.sa_data, sizeof(struct sockaddr));

	return 0;
}

static void freepeers(lc_relay_t *rel)
{
	lc_peer_t *tmp, *ptr;
	for (ptr = rel->peer; ptr; tmp = ptr, ptr = ptr->next, free(tmp));
	rel->peer = NULL;
}

static int peer_add(lc_relay_t *rel, struct sockaddr *sa, size_t socklen, char type)
{
	lc_peer_t *peer = calloc(1, sizeof(lc_peer_t));
	if (!peer) return -1;
	memcpy(&peer->addr, sa, socklen);
	peer->type = type;
	clock_gettime(CLOCK_REALTIME, &peer->seen);
	if (!rel->peer) {
		rel->peer = peer;
	}
	else for (lc_peer_t *ptr = rel->peer; ptr; ptr = ptr->next) {
		if (!ptr->next) {
			ptr->next = peer;
			break;
		}
	}
	return 0;
}

#define peer_get peer_gynt
static lc_peer_t *peer_gynt(lc_relay_t *rel, struct sockaddr *sa)
{
	for (lc_peer_t *peer = rel->peer; peer; peer = peer->next) {
		if (!memcmp(&peer->addr, sa, sizeof(struct sockaddr_in)))
			return peer;
	}
	return NULL;
}

static void examine_headers(char *buf, struct sockaddr_in6 *dst)
{
	char straddr[INET6_ADDRSTRLEN];
	struct ip6_hdr *ip6h = (struct ip6_hdr *)buf;
	char *ptr = ((char *)ip6h) + (ssize_t)sizeof(struct ip6_hdr);
	struct udphdr *udph = NULL;

	inet_ntop(AF_INET6, &ip6h->ip6_dst, straddr, INET6_ADDRSTRLEN);
	fprintf(stderr, "\tdst %s\n", straddr);

	for (uint8_t nxt = ip6h->ip6_nxt; nxt != 255; ) {
		switch (nxt) {
		case 0: /* Hop-by-Hop options header */
			ptr += ((struct ip6_hbh *)ptr)->ip6h_len * 8;
			nxt = ((struct ip6_hbh *)ptr)->ip6h_nxt;
			break;
		case 17: /* UDP */
			udph = (struct udphdr *)ptr;
			nxt = 255;
			break;
		case 58: /* ICMPv6 */
			nxt = 255;
			break;
		default:
			nxt = 255;
			break;
		}
	}
	if (dst)
		memcpy(&dst->sin6_addr, &ip6h->ip6_dst, sizeof(struct in6_addr));

	if (udph) {
		fprintf(stderr, "\tsport: %u (udphdr)\n", ntohs(udph->uh_sport));
		fprintf(stderr, "\tdport: %u (udphdr)\n", ntohs(udph->uh_dport));
		if (dst) dst->sin6_port = udph->uh_dport;
	}
}

static void *listen_wan(void *arg)
{
	lc_relay_t *rel = arg;
	lc_peer_t *peer;
	char straddr[INET6_ADDRSTRLEN];
	char buf[1500] = {0};
	struct ethhdr eth = {0};
	struct sockaddr_storage src = {0};
	struct sockaddr_in6 dst = {0};
	struct iovec iov[2];
	struct msghdr msg = {0};
	ssize_t rc;

	iov[0].iov_base = &eth;
	iov[0].iov_len = sizeof eth;
	iov[1].iov_base = buf;
	msg.msg_name = &src;
	msg.msg_namelen = sizeof src;
	msg.msg_iov = iov;
	msg.msg_iovlen = sizeof iov / sizeof iov[0];

	while (running) {
		iov[1].iov_len = sizeof buf;
		rc = recvmsg(rel->if_wan.sock, &msg, 0);
		if (rc > 0) {
			inet_ntop(AF_INET, &((struct sockaddr_in *)&src)->sin_addr, straddr, INET6_ADDRSTRLEN);
			fprintf(stderr, "WAN: %zi bytes received from %s\n", rc, straddr);
			peer = peer_gynt(rel, (struct sockaddr *)&src);
			if (peer) {
				fprintf(stderr, "\trefreshing peer %s\n", straddr);
				clock_gettime(CLOCK_REALTIME, &peer->seen);
			}
			else {
				/* register peer */
				fprintf(stderr, "\tregistering peer %s\n", straddr);
				peer_add(rel, (struct sockaddr *)&src, sizeof(struct sockaddr_in), 1);
			}
			examine_headers(buf, &dst);

			/* deencapsulate data & send on TUN device */

			/* set source MAC address */
			dumpmac(stderr, "\tmac (old)", eth.h_source);
			memcpy(eth.h_source, &rel->if_tun.hwaddr, ETH_ALEN * sizeof (uint8_t));
			dumpmac(stderr, "\tmac (src)", eth.h_source);
			dumpmac(stderr, "\tmac (dst)", eth.h_dest);

			struct sockaddr_ll sll = {
				.sll_family = AF_PACKET,
				.sll_ifindex = rel->if_tun.ifx,
				.sll_halen = ETH_ALEN,
				.sll_protocol = htons(ETH_P_IPV6)
			};
			memcpy(sll.sll_addr, eth.h_dest, ETH_ALEN);
			msg.msg_name = &sll;
			msg.msg_namelen = sizeof(struct sockaddr_ll);
			msg.msg_iov[1].iov_len = rc - sizeof eth;
			inet_ntop(AF_INET6, &((struct sockaddr_in6 *)&dst)->sin6_addr, straddr, INET6_ADDRSTRLEN);
			fprintf(stderr, "\tWAN: decap packet forwarding to [%s]:%u\n", straddr,
					ntohs(dst.sin6_port));

			rc = sendmsg(rel->if_tun.sock, &msg, 0);
			if (rc == -1)
				perror("WAN: sendmsg");
			else if (rc > 0)
				fprintf(stderr, "WAN: %zi bytes sent to TUN %s\n", rc, straddr);
		}
	}

	return arg;
}

static void *listen_tun(void *arg)
{
	lc_relay_t *rel = arg;
	char buf[1500] = {0};
	char straddr[INET6_ADDRSTRLEN];
	struct ethhdr eth = {0};
	struct sockaddr_in *dst;
	struct sockaddr_ll src = {0};
	struct iovec iov[2];
	struct msghdr msg = {0};
	ssize_t rc;
	iov[0].iov_base = &eth;
	iov[0].iov_len = sizeof eth;
	iov[1].iov_base = buf;
	while (running) {
		msg.msg_name = &src;
		msg.msg_namelen = sizeof(struct sockaddr_storage);
		msg.msg_iov = iov;
		msg.msg_iovlen = sizeof iov / sizeof iov[0];
		iov[1].iov_len = sizeof buf;
		rc = recvmsg(rel->if_tun.sock, &msg, 0);
		if (rc == -1) {
			perror("recvmsg");
		}
		else if (rc > 0) {
			char ifname[IFNAMSIZ];
			if_indextoname(src.sll_ifindex, ifname);
			fprintf(stderr, "TUN: %zi bytes received on %s(%i)\n", rc, ifname, src.sll_ifindex);
			if (ntohs(src.sll_protocol) != ETHERTYPE_IPV6) continue;

			examine_headers(buf, NULL);

			/* encapsulate data, send to peers */
			for (lc_peer_t *peer = rel->peer; peer; peer = peer->next) {
				dst = (struct sockaddr_in *)&peer->addr;
				inet_ntop(AF_INET, &dst->sin_addr, straddr, INET6_ADDRSTRLEN);
				msg.msg_name = dst;
				msg.msg_namelen = sizeof(struct sockaddr_in);
				iov[1].iov_len = rc - sizeof eth;
				rc = sendmsg(rel->if_wan.sock, &msg, 0);
				if (rc == -1) {
					perror("sendto");
				}
				else if (rc > 0) {
					fprintf(stderr, "TUN: %zi bytes sent to relay %s\n", rc, straddr);
				}
			}
		}
	}
	return arg;
}

static int peer_add_str(lc_relay_t *rel, char *strpeer)
{
	struct sockaddr_in sa = {0};
	fprintf(stderr, "relay: %s\n", strpeer);
	if (inet_pton(AF_INET, strpeer, &sa.sin_addr) == -1) {
		return -1;
	}
	sa.sin_family = AF_INET;
	sa.sin_port = htons(RELAY_PORT_UDP);
	return peer_add(rel, (struct sockaddr *)&sa, sizeof(struct sockaddr_in), 0);
}

static inline void setrcvbufsize(int sock, int sz)
{
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUFFORCE, &sz, sizeof sz) == -1) {
		perror("setsockopt(SO_RCVBUF)");
	}
}

static int relay_start(int argc, char *argv[])
{
	lc_relay_t rel = {0};
	struct sockaddr_ll addr = {
		.sll_family = AF_PACKET,
		.sll_protocol = htons(ETH_P_ALL)
	};
	char straddr[INET6_ADDRSTRLEN];
	struct ifaddrs *ifaddr, *wan_ifa = NULL;
	pthread_t tid[2];
	int fdtun, err;

	/* add static peers */
	for (int i = (bridge) ? 1 : 2; i < argc; i++) {
		peer_add_str(&rel, argv[i]);
	}

	/* initialize tunnel interface */
	if (bridge) {
		memset(rel.if_tun.ifname, 0, sizeof rel.if_tun.ifname);
		rel.ctx = lc_ctx_new();
		if ((err = lc_bridge_add(rel.ctx, bridge)) && err != EEXIST) {
			fprintf(stderr, "lc_bridge_add: %s\n", strerror(err));
		}
		lc_link_set(rel.ctx, bridge, 1);
		fdtun = lc_tap_create(rel.if_tun.ifname);
		if (fdtun == -1) {
			perror("lc_tuntap_create");
			return -1;
		}
		if ((err = lc_bridge_addif(rel.ctx, bridge, rel.if_tun.ifname))) {
			fprintf(stderr, "lc_bridge_add: %s\n", strerror(err));
			return -1;
		}
		lc_link_set(rel.ctx, rel.if_tun.ifname, 1);
		fprintf(stderr, "created tap '%s'\n", rel.if_tun.ifname);
	}
	else {
		/* use first argument as tunnel interface */
		strcpy(rel.if_tun.ifname, argv[0]);
	}
	fprintf(stderr, "TUN interface: %s\n", rel.if_tun.ifname);

	/* WAN interface */
	if (getifaddrs(&ifaddr)) {
		perror("getifaddrs()");
		return -1;
	}
	for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		if (strcmp(wan_iface, ifa->ifa_name)) continue;
		if (ifa->ifa_addr->sa_family != AF_INET) continue; /* IPv4 */
		wan_ifa = ifa;
		break;
	}
	if (!wan_ifa) {
		fprintf(stderr, "IPv4 address for WAN interface '%s' not found\n", wan_iface);
		return -1;
	}
	strncpy(rel.if_wan.ifname, wan_ifa->ifa_name, IFNAMSIZ);

	inet_ntop(wan_ifa->ifa_addr->sa_family, get_in_addr(wan_ifa->ifa_addr), straddr, INET6_ADDRSTRLEN);
	fprintf(stderr, "binding to %s\n", straddr);

	if ((rel.if_wan.sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket(WAN)");
		return -1;
	}
	((struct sockaddr_in *)wan_ifa->ifa_addr)->sin_port = htons(RELAY_PORT_UDP);
	if (bind(rel.if_wan.sock, wan_ifa->ifa_addr, sizeof(struct sockaddr_in)) == -1) {
		perror("bind(WAN)");
		return -1;
	}

	rel.if_tun.sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

	setrcvbufsize(rel.if_wan.sock, INT_MAX);
	setrcvbufsize(rel.if_tun.sock, INT_MAX);

	if (interface_init(&rel.if_wan)) return -1;
	if (interface_init(&rel.if_tun)) return -1;

	addr.sll_ifindex = rel.if_tun.ifx;
	if (bind(rel.if_tun.sock, (struct sockaddr *)&addr, sizeof addr) == -1) {
		perror("bind(TUN)");
		return -1;
	}

	/* create worker threads */
	pthread_create(&tid[WAN], NULL, &listen_wan, &rel);
	pthread_create(&tid[TUN], NULL, &listen_tun, &rel);

	/* control thread */
	signal(SIGINT, &handle_sigint);
	pause();

	pthread_cancel(tid[WAN]);
	pthread_cancel(tid[TUN]);
	pthread_join(tid[WAN], NULL);
	pthread_join(tid[TUN], NULL);
	close(rel.if_wan.sock);
	close(rel.if_tun.sock);
	freeifaddrs(ifaddr);
	freepeers(&rel);
	close(fdtun);
	lc_ctx_free(rel.ctx);

	return 0;
}

int process_opts(int argc, char *argv[])
{
	int i;
	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--bridge")) {
			if (argv[i + 1]) {
				i++;
				bridge = argv[i];
				fprintf(stderr, "bridge: %s\n", bridge);
			}
			else usage(EXIT_FAILURE);
		}
		else break;
	}
	return i;
}

int main(int argc, char *argv[])
{
	int arg0 = 1; /* index of first argument after opts */
	progname = basename(argv[0]);
	arg0 = process_opts(argc, argv);
	if (!argv[arg0]) usage(EXIT_FAILURE);
	if (argc - arg0 > 2) {
		/* we are a gateway */
		fprintf(stderr, "gateway mode\n");
	}
	else {
		/* relay */
		fprintf(stderr, "relay mode\n");
	}
	wan_iface = (bridge) ? argv[arg0] : argv[arg0 + 1];
	if (!wan_iface) usage(EXIT_FAILURE);
	fprintf(stderr, "WAN interface: %s\n", wan_iface);
	if (relay_start(argc - arg0, &argv[arg0]) == -1) {
		fprintf(stderr, "failed to start relay\n");
	}
	return 0;
}

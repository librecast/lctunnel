# lctunnel - Librecast Multicast Tunnel Daemon

`lctunnel` connects two IPv6 multicast networks using a UDP tunnel.
It is being developed to support the Librecast Project's R&D
projects as part of the NGI Zero (Next Generation Internet) Programme.

`lctunnel` is not fully RFC-compliant for AMT (RFC 7450).  AMT is unidirectional,
and assumes that all multicast sources are at the "relay" end of the tunnel.
That does not fit with our many-to-many multicast development.

`lctunnel` is designed to work with `lcroute`, but can be used separately.

## Status

In development.  Pre-alpha.

## Build & Install

Dependencies:
- librecast

`make`

`make install`

## Usage

`lctunnel [-b bridge] [--rp] [tun_iface] wan_iface [relay_ip] ...`

The `-b` option will create `bridge` as a virtual bridge if it does not exist,
and create and connect a tap interface to that bridge.

tun_iface is the interface on which IPv6 multicast traffic will be sent and
received.  This is required unless running in `-b` bridge mode.

wan_iface is the interface that will be used for the UDP tunnel.

If one or more relay_ip are specified, `lctunnel` operates in gateway mode, and
will tunnel traffic received on tun_iface to the relay. Otherwise the daemon
operates in "relay" mode, and waits for a gateway to connect.

"Connect" is used quite loosely here, as there is no handshake or connection, as
such.  When a relay receives traffic from a gateway it has not seen before it
adds it as a peer and sets a freshness timer.

## Security

Presently, none.  Access control is using ebtables/iptables only.  An upcoming
release will add authentication.

All traffic is sent with no additional encryption. It is expected that the
underlying multicast traffic will already be end-to-end or conference-key
encrypted.  This is NOT a VPN.  We may later add Wireguard or similar support,
but don't want this to be the default mode, as there's no need for this
additional overhead for many use-cases. You could run the tunnel traffic through
a VPN separately if required.

## Questions, Bug reports, Feature Requests

New issues can be raised at:

https://github.com/librestack/lctunnel/issues

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.

<hr />

### IRC channel

`#librecast` on libera.chat

If you have a question, please be patient. An answer might take a few hours
depending on time zones and whether anyone on the team is available at that
moment. 

<hr />

## License

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

<hr />

# Funding

<p class="bigbreak">
This project was funded through the <a href="https://nlnet.nl/discovery"> NGI0 Discovery </a> Fund, a fund established by NLnet with financial support from the European
Commission's <a href="https://ngi.eu">Next Generation Internet</a> programme, under the aegis of DG Communications Networks, Content and Technology under grant agreement No 825322. *Applications are still open, you can <a href="https://nlnet.nl/propose">apply today</a>*
</p>

  <a href="https://nlnet.nl/project/LibrecastLive/">
      <img width="250" src="https://nlnet.nl/logo/banner.png" alt="Logo NLnet: abstract logo of four people seen from above" class="logocenter" />
  </a>
  <a href="https://ngi.eu/">
      <img width="250" align="right" src="https://nlnet.nl/image/logos/NGI0_tag.png" alt="Logo NGI Zero: letterlogo shaped like a tag" class="logocenter" />
  </a>
</p>

# Overview

Our aim is to connect to the native Librecast stratum 0 backbone based on IPv6 multicast.

First we need to check if we already are.

First we check if we have IPv6 addresses assigned to any of our interfaces.  If
not, it's safe to assume we do not have IPv6 connectivity.  If yes, we send a
Librecast Hello message to the All Librecast Routers address.  Routers will
respond with our stratum level.  If no response, then we have IPv6, but not
Librecast.

If we do not have Librecast connectivity, we need to set up a tunnel back to
somewhere that does.

We will determine which relay(s) to connect to (initially statically configured)
and tunnel to them over UDP (IPv6 or IPv4).  This will punch through NAT as
required.  If UDP is blocked, we can fall back to TCP.
